import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';



/*
  Generated class for the VilleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VilleProvider {
  private apiUrl ="https://geo.api.gouv.fr/communes?nom=Maur&fields=nom,codesPostaux&format=json&geometry=centre"
  constructor(public http: HttpClient) {
  }
  getVille(name : String){
    return this.http.get("https://geo.api.gouv.fr/communes?nom=" + name + "&fields=nom,codesPostaux&format=json&geometry=centre")//.map(res => res).subscribe(data => {
     
  }

  

}
