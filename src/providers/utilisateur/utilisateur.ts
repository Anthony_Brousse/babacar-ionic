import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { MesTrajetsPage } from '../../pages/mes-trajets/mes-trajets'

import * as firebase from 'firebase';
import 'firebase/firestore';
/*
  Generated class for the TrajetProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilisateurProvider {
  private _DB : any;
  private collectionObj : String;

  constructor() {
    this._DB = firebase.firestore();
    this.collectionObj = "Utilisateur"
  }
  getDocuments() : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj)
      .get()
      .then((querySnapshot) => {
        let obj : any = [];
        querySnapshot
        .forEach((doc: any) => {
          obj.push({
           id            : doc.id,
           nom           : doc.data().nom,
           prenom        : doc.data().date,
           nbPlace       : doc.data().prenom,
           ddn           : doc.data().ddn,
           telephone     : doc.data().telephone,
           mail          : doc.data().mail,
          });
        });

        resolve(obj);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

  addDocument(user,n,p) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(user.uid).set({
        mail : user.email,
        nom : n,
        prenom : p
      });
    });
  }

  addDocument2(uid,email,n,p) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(uid).set({
        mail : email,
        nom : n,
        prenom : p
      });
       
    });
  }

  /*db.collection("cities").doc("LA").set({
    name: "Los Angeles",
    state: "CA",
    country: "USA"
})*/

  deleteDocument(docID : string) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj)
      .doc(docID)
      .delete()
      .then((obj : any) => {
        resolve(obj);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

/**
* Update an existing document within a selected database collection
*/

updateDocument(docID : string,
      dataObj : any) : Promise<any>{
    return new Promise((resolve, reject) => {
    this._DB.collection(this.collectionObj)
      .doc(docID)
      .update(dataObj)
      .then((obj : any) => {
        resolve(obj);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }


 getUtilisateurByID(idUser) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(idUser)
      .get()
      .then((query) => {
        var user = {
          nom : query.data().nom,
          prenom : query.data().prenom,
          mail : query.data().mail,
          id: idUser
        }
        resolve(user);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

  getOneUtilisateur(id){
    return this._DB.doc(this.collectionObj +'/' + id);
  }

  addTrajet(id, trajet) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Trajets").doc(trajet.id).set({
        trajet,
      })
    });
  }

  removeTrajet(id,trajet) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Trajets").doc(trajet.id).delete()
      .then((obj: any)=>{
        resolve(obj)
      })
      .catch((error: any)=> {
        reject(error)
      })

    });
  }

  getTrajets(id, page: MesTrajetsPage ) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Trajets")
      .get()
      .then((query) => {
        let trajets = []
        query.forEach((doc: any) => {
            this.getOneTrajet(doc.get("trajet").id, page)
            .then(trajet=> {
                trajets.push(trajet)
                
             })
        })
        resolve(trajets);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

  getOneTrajet(id, page: MesTrajetsPage ) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection("Trajet").doc(id)
      .get()
      .then((doc) => {
        var trajet = {
          id             : doc.id,
          conducteur     : null,
          date           : doc.data().date,
          nbPlace        : doc.data().nbPlace,
          prix           : doc.data().prix,
          ville_depart   : doc.data().ville_depart,
          ville_arrive   : doc.data().ville_arrive,
          passagers      : []
        }
        if(doc.data().conducteur != undefined){
          this.getUtilisateurByID(doc.data().conducteur.id).then(data =>{
              trajet.conducteur = data
              this.getPassagers(doc.id).then(data => {
                trajet.passagers = data
                page.notify();
               });
            })
            
        }
        
        
        resolve(trajet)
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }
 

  getPassagers(id) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection("Trajet").doc(id).collection("Passagers")
      .get()
      .then((query) => {
        let passagers: any = []
        query
        .forEach((doc: any) => {
            this.getUtilisateurByID(doc.get("passager").id)
            .then(user=> {
              passagers.push(user)
            })
        })
        resolve(passagers);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }
}



