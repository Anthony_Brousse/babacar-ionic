import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


/*
  Generated class for the LocalisationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalisationProvider {



  constructor(public http: HttpClient) {
  }
  //  https://nominatim.openstreetmap.org/reverse?format=xml&lat=45.771263999999995&lon=3.1198023&zoom=18&addressdetails=1
  getVille(lat,long){
    return this.http.get("https://nominatim.openstreetmap.org/reverse?format=json&lat=" + lat + "&lon="+long +"&zoom=18&addressdetails=1")
         
  }

}
