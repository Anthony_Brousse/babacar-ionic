import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { UtilisateurProvider }  from "../../providers/utilisateur/utilisateur";

import * as firebase from 'firebase';
import 'firebase/firestore';
import { query } from '@angular/core/src/render3/instructions';
/*
  Generated class for the TrajetProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TrajetProvider {
  private _DB : any;
  private collectionObj : String;

  constructor(private utilisateurProvider: UtilisateurProvider ) {
    this._DB = firebase.firestore();
    this.collectionObj = "Trajet"
  }
  getDocuments() : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj)
      .get()
      .then((querySnapshot) => {
        let obj : any = [];
        let passagers : any = [];
        querySnapshot
        .forEach((doc: any) => {
          passagers = []
          var elmt = {
            id             : doc.id,
            conducteur     : null,
            date           : doc.data().date,
            nbPlace        : doc.data().nbPlace,
            prix           : doc.data().prix,
            ville_depart   : doc.data().ville_depart,
            ville_arrive   : doc.data().ville_arrive,
            passagers      : []
           }
          obj.push(elmt);
          if(doc.data().conducteur != undefined){
            this.utilisateurProvider.getUtilisateurByID(doc.data().conducteur.id).then(data =>{
                elmt.conducteur = data
              })
          }
          
          this.getPassagers(doc.id).then(data => {
            elmt.passagers = data
           });
          })
         
          resolve(obj);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }


  getPassagers(id) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Passagers")
      .get()
      .then((query) => {
        let passagers: any = []
        query
        .forEach((doc: any) => {

            this.utilisateurProvider.getUtilisateurByID(doc.get("passager").id)
            .then(user=> {
              passagers.push(user)
            })
        })
        resolve(passagers);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

  addPassager(id, passager) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Passagers").doc(passager.id).set({
        passager,
      })
    });
  }

  removePassager(id,passager) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj).doc(id).collection("Passagers").doc(passager.id).delete()
      .then((obj: any)=>{
        resolve(obj)
      })
      .catch((error: any)=> {
        reject(error)
      })

    });
  }


  addDocument(dataObj : any) : Promise<any>{
    return new Promise((resolve, reject) => {
        this._DB.collection(this.collectionObj).add(dataObj)
        .then((obj : any) => {
          resolve(obj);
        })
      .catch((error : any) => {
        reject(error);
      });
    }); 
  }


  deleteDocument(docID : string) : Promise<any>{
    return new Promise((resolve, reject) => {
      this._DB.collection(this.collectionObj)
      .doc(docID)
      .delete()
      .then((obj : any) => {
        resolve(obj);
      })
      .catch((error : any) => {
        reject(error);
      });
    });
  }

/**
* Update an existing document within a selected database collection
*/

updateDocument(docID : string,
      dataObj : any) : Promise<any>{
    return new Promise((resolve, reject) => {
    this._DB.collection(this.collectionObj)
    .doc(docID)
    .update(dataObj)
    .then((obj : any) => {
       resolve(obj);
    })
    .catch((error : any) => {
      reject(error);
    });
    });
    
  }

  




}

