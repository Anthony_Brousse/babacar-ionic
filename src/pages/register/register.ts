import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import firebase, { firestore } from 'firebase';
import { LoginPage } from '../login/login';
import {UtilisateurProvider} from '../../providers/utilisateur/utilisateur'
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  password = "";
  password2 = "";
  email = '';
  nom = '';
  prenom = '';
  loading;
  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public db_utilisateur : UtilisateurProvider) {
  }

  ionViewDidLoad() {
  }


 
  public create() {
    var errorMessage = undefined;
    this.showLoading()
    if(this.password != this.password2){
      this.showError("error create Account","the 2 passwords are different")
    }else{
      firebase.auth().createUserWithEmailAndPassword(this.email, this.password).then((result) => {
        this.loading.dismiss();
        this.createUser(result.user)
      }).catch((error) => {
        var errorCode = error.code;
        errorMessage = error.message;
        this.showError(error.code,errorMessage)
      });
    }
    
  }

  createUser(user){   
    this.db_utilisateur.addDocument(user,this.nom,this.prenom)
    .then(function() {
  })
  this.navCtrl.push(TabsPage);
}
 
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(code,text) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: code,
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}