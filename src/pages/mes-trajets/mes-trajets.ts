import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UtilisateurProvider} from '../../providers/utilisateur/utilisateur';
import { LoadingController } from 'ionic-angular';
import firebase, { firestore } from 'firebase';
import { ShowTrajetPage } from '../show-trajet/show-trajet'


/**
 * Generated class for the MesTrajetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mes-trajets',
  templateUrl: 'mes-trajets.html',
})
export class MesTrajetsPage {

  allTrajets = [];
  mesTrajets = [];
  userId = null;
  show: boolean = null
  load: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams, private utilisateurProvider: UtilisateurProvider,public loadingCtrl: LoadingController) {
      
  }

  ionViewWillEnter(){
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    this.userId = firebase.auth().currentUser.uid;
    this.show = false;
    this.loadTrajets()
  }


  loadTrajets(){
    this.mesTrajets = []
  this.utilisateurProvider.getTrajets(firebase.auth().currentUser.uid, this)
  .then((data)=>{
    this.mesTrajets = data;
  })

  }

  
  notify(){
    this.show= true
  }

  selectTrajet(trajet){
    this.navCtrl.push(ShowTrajetPage, {
      trajet: trajet,
    })
  }


}
