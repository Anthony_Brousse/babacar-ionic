import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreationTrajetPage } from './creation-trajet';

@NgModule({
  declarations: [
    CreationTrajetPage,
  ],
  imports: [
    IonicPageModule.forChild(CreationTrajetPage),
  ],
})
export class CreationTrajetPageModule {}
