import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VilleProvider } from "../../providers/ville/ville";
import { ToastController } from 'ionic-angular';
import {TrajetProvider} from '../../providers/trajet/trajet';
import {LocalisationProvider} from '../../providers/localisation/localisation';
import {UtilisateurProvider} from '../../providers/utilisateur/utilisateur';

import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs'
import firebase, { firestore } from 'firebase';

/**
 * Generated class for the CreationTrajetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-creation-trajet',
  templateUrl: 'creation-trajet.html',
})
export class CreationTrajetPage {

  public localisationList:Array<any>;
  public loadedLocalisationList:Array<any>;
  public showList : boolean;
  boolDepart : boolean =false;
  boolArrivee : boolean =false;
  boolDate : boolean =false;
  boolPrix : boolean = false;
  boolPlace: boolean = false;

  public cities:Array<string> = [];
  date ='';
  depart = '';
  arrivee = '';
  city ='';
  holder = '';
  minDate ='';
  prix: number ;
  place: number;

  constructor(public navCtrl: NavController,
             public navParams: NavParams,
             private db_Ville : VilleProvider,
             private toastCtrl : ToastController, 
             private db_trajet :TrajetProvider , 
             private geoloc: Geolocation, 
             private nativeGeoco : NativeGeocoder,
             public db_utilisateur : UtilisateurProvider,
             public db_localisation : LocalisationProvider){
    this.showList =false
    this.holder = "De :" ;
    this.minDate = new Date().toISOString();
  }


  ionViewWillEnter(){
    this.boolArrivee = false;
    this.boolDepart = false;
    this.boolDate = false;
    this.boolPrix = false;
    this.boolPlace =false;
    this.prix= null ;
    this.place = null;
    this.date ='';
    this.depart = '';
    this.arrivee = '';
    this.city ='';
    this.holder = '';
  }



  initializeItems(): void {
    this.localisationList = this.loadedLocalisationList;
    this.showList = true;
  }

  getVilles(name : String){
    //return this.db_Ville.getVille(name);
    this.db_Ville.getVille(name)
            .subscribe((data: any[]) =>{
            this.localisationList = data.slice(0,5)
          });
  }

  selectItem(localisation){
    this.city = ''
    if(!this.boolDepart){
      this.depart = localisation.nom;
      this.holder =" A : "
      this.boolDepart = true;
    }else{
      this.arrivee = localisation.nom
      this.boolArrivee = true;
    }
    this.showList = false;
  }

  getItems(searchbar) {
    this.initializeItems();
  
    var q = searchbar.srcElement.value;
  
  
    if (!q) {
      this.showList = false;
      return;
    }
    this.showList = true;
    this.getVilles(q);   
  }

  onclicksearch(searchbar){
    if(this.boolDepart){
      searchbar.srcElement.value = '';
    }
  }

  delete(chip, b) {
    if(chip != null){
      chip.remove();
    }
    switch(b){
      case 'dep': this.boolDepart = false;
               this.depart=''; 
               this.holder = 'De :'
               break;
      case 'arr': this.boolArrivee = false;
              this.arrivee='' ;
              this.holder = 'A :'
              break;
      case 'date' : this.boolDate = false;  this.date ='';break;
      case 'prix' : this.boolPrix = false; this.prix= null; break; 
      case 'place' : this.boolPlace = false; this.place = null; break;
    }
  }

  onselectdate(){
    if(this.date!= ''){
      this.boolDate =true;
    }
    
  }

  pricechange(){
    if(this.prix>0){
      this.boolPrix = true
    }else{
        let toast = this.createToast('Vous devez sélectionner un prix supérieurs à 0', 3000, 'top')
        toast.present()
    }
  }

  placechange(){
    if(this.place>0){
      this.boolPlace = true
    }else{
        let toast = this.createToast('Vous devez sélectionner un nombre de places supérieurs à 0', 3000, 'top')
        toast.present()
    }
  }
  
 
  creerTrajet(){   
     let obj ={
       date           : this.date,
       nbPlace        : this.place,
       prix           : this.prix,
       ville_arrive   : this.arrivee,
       ville_depart   : this.depart,
       conducteur     : null,
      };
      obj.conducteur = this.db_utilisateur.getOneUtilisateur(firebase.auth().currentUser.uid);
     this.db_trajet.addDocument(obj)
     .then(res =>{ 
       this.db_utilisateur.addTrajet(firebase.auth().currentUser.uid, res)     
     });     
      this.navCtrl.push(HomePage,{creation : "oui", message : 'Votre trajet a été créé avec succès'});
   }

  createToast(msg, duration, position){
    return this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,
    })
  }

  

  getLocalisation(){
     let watch = this.geoloc.watchPosition();
     watch.subscribe((data) => {
        this.db_localisation.getVille(data.coords.latitude,data.coords.longitude)
        .subscribe((data: any) =>{
          if(data.address.city != undefined){
            this.city = data.address.city; 
          }else{
            this.city = data.address.town; 
          }
          this.selectItem({nom : this.city, code : data.address.postcode})              
        });
     });
  
     
  }

}
