import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TrajetProvider }  from "../../providers/trajet/trajet";
import { VilleProvider } from "../../providers/ville/ville";
import { viewClassName } from '@angular/compiler';
import { ShowTrajetPage } from '../show-trajet/show-trajet'
import { ToastController } from 'ionic-angular';
import firebase, { firestore } from 'firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public localisationList:Array<any>;
  public loadedLocalisationList:Array<any>;
  public showList : boolean;
  public b : boolean;
  public showFilterVD = false;
  public showFilterVA = false;
  public showSearchBarVD = true;
  public showSearchBarVA = false;
  public showFilterDate = false;
  public showDate = false;


  villeDepart = ''
  villeArrive = ''
  public myDate;
  public trajets = [];

  constructor(public navCtrl: NavController,
              private db_Ville : VilleProvider,
              private db_trajet : TrajetProvider,
              public navParams :NavParams,
              public toastCtrl : ToastController) {    

    this.showList= false;
    if(this.navParams.get("creation") != undefined){
      var message = this.navParams.get("message")
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
  }

  

  initializeItems(): void {
    this.localisationList = this.loadedLocalisationList;
    this.showList = true;
  }

  getVilles(name : String){
    this.db_Ville.getVille(name)
            .subscribe((data: any[]) =>{
            this.localisationList = data.slice(0,5)
          });
  }

  selectItem(localisation){
    if(this.b){
      this.villeDepart = localisation.nom;
      this.showFilterVD = true;
      this.showSearchBarVD = false;
      this.recherche();
      this.showSearchBarVA = true;
    }else{
      this.villeArrive = localisation.nom
      this.showFilterVA = true;
      this.showSearchBarVA = false;
      this.recherche();
      this.showDate = true;
    }
    this.showList = false;
  }
  getItems(searchbar,b) {
    this.b = b;
    this.initializeItems();
  
    var q = searchbar.srcElement.value;
  
  
    if (!q) {
      this.showList = false;
      return;
    }
    this.showList = true;
    this.getVilles(q);   
  }

  recherche(){
    var villeDepart = this.villeDepart;
    var villeArrive = this.villeArrive;
    var date = this.myDate;
    this.getTrajet(villeDepart,villeArrive,date);
    

    //rechercher les trajet selons les critère
  }
  /**
   * Retourne les trajets correspondant à la recherche
   * @param villeDepart 
   * @param villeArrive 
   * @param date 
   */
  getTrajet(villeDepart,villeArrive,date) : void{
    this.trajets = [];
    this.db_trajet.getDocuments()    
    .then((data) =>
    {
      data.forEach(element => {
        if(element.ville_depart == villeDepart){
          if(element.ville_arrive == villeArrive){
            if(date == undefined){
              this.trajets.push(element)
            }else if(element.date == date){
              this.trajets.push(element)             
            }
          }
        }
      });

    });
  }

  deleteChipVD(chip: Element) {
    this.showSearchBarVD = true;
    this.villeDepart = "";
    this.showFilterVD = false;
    this.trajets = [];
    if(this.showSearchBarVA){
      this.showSearchBarVA = false;
    }
    chip.remove();
  }

  deleteChipVA(chip: Element) {
    this.showSearchBarVA = true;
    this.villeArrive = "";
    this.showFilterVA = false;
    this.trajets = [];
    if(this.showSearchBarVD){
      this.showSearchBarVA = false;
    }
    chip.remove();
  }


  deleteChipDate(chip: Element) {
    this.showDate = true;
    this.showFilterDate = false;
    chip.remove();
  }
  
  dateTimeChange(event){
    this.showDate = false;
    this.showFilterDate = true;
    this.recherche();
  }

  selectTrajet(trajet){
    this.navCtrl.push(ShowTrajetPage, {
      trajet: trajet,
    })
  }
}
