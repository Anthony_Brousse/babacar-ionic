import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowTrajetPage } from './show-trajet';

@NgModule({
  declarations: [
    ShowTrajetPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowTrajetPage),
  ],
})
export class ShowTrajetPageModule {}
