import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{TrajetProvider} from '../../providers/trajet/trajet';
import firebase, { firestore } from 'firebase';
import {UtilisateurProvider} from '../../providers/utilisateur/utilisateur';
import {HomePage} from '../home/home'
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the ShowTrajetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-trajet',
  templateUrl: 'show-trajet.html',
})

export class ShowTrajetPage {
  public trajet = null;
  alreadyPassager : boolean = false
  conducteur : boolean 
  idPassager: String

  constructor(public navCtrl: NavController, public navParams: NavParams, private db_trajet :TrajetProvider, private db_utilisateur : UtilisateurProvider, private toastCtrl : ToastController) {
    this.trajet = navParams.get("trajet");
    this.conducteur = this.trajet.conducteur.id == firebase.auth().currentUser.uid
    this.checkIfAlreadyPassager()
  }

  reserverPlace(){
    this.db_trajet.addPassager(this.trajet.id, this.db_utilisateur.getOneUtilisateur(firebase.auth().currentUser.uid))
    this.db_utilisateur.addTrajet(firebase.auth().currentUser.uid, this.trajet)
    this.navCtrl.push(HomePage)
    this.createToast("Réservation effectuée avec succès", 3000, "top").present()
  }

  checkIfAlreadyPassager(){
    this.trajet.passagers.forEach(element => {
      if(element.id == firebase.auth().currentUser.uid){
        this.idPassager = element.idPassager
        this.alreadyPassager = true;
      }
    })
  }


  annulerReservation(){
    this.db_trajet.removePassager(this.trajet.id,this.db_utilisateur.getOneUtilisateur(firebase.auth().currentUser.uid)  )
    this.db_utilisateur.removeTrajet(firebase.auth().currentUser.uid, this.trajet)
    this.navCtrl.push(HomePage)
    this.createToast("Réservation annulée avec succès", 3000, "top").present()
  }

  annulerTrajet(){
    this.db_trajet.deleteDocument(this.trajet.id)
    this.db_utilisateur.removeTrajet(firebase.auth().currentUser.uid, this.trajet)
    this.navCtrl.push(HomePage)
    this.createToast("Trajet annulé ave succès", 3000, "top").present()
  }

  createToast(msg, duration, position){
    return this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: position,
    })
  }



 

}
