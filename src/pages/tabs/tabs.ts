import { Component } from '@angular/core';

import { MesTrajetsPage } from '../mes-trajets/mes-trajets';
import { CreationTrajetPage } from '../creation-trajet/creation-trajet';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = MesTrajetsPage;
  tab3Root = CreationTrajetPage;

  constructor() {

  }
}
