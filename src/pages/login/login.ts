import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import firebase from 'firebase';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import {UtilisateurProvider} from '../../providers/utilisateur/utilisateur'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  password = "";
  email = '';
  loading;
  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
     public navParams: NavParams,
     public loadingCtrl: LoadingController,
     public db_utilisateur : UtilisateurProvider) {
  }


public createAccount() {
    this.navCtrl.push(RegisterPage);
  }
 
  public login() {
    var errorMessage = undefined;
    this.showLoading()
    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then((result) => {
      this.loading.dismiss();
      this.navCtrl.push(TabsPage);
    }).catch((error) => {
      var errorCode = error.code;
      errorMessage = error.message;
      this.showError(error.code,errorMessage)
    });
  }
  
  public googleLogin(){  
    var provider = new firebase.auth.GoogleAuthProvider();
    var errorMessage = undefined; 
    var errorCode = undefined;
    this.showLoading()
    firebase.auth().signInWithPopup(provider).then((result) => {
      this.loading.dismiss();
      this.db_utilisateur.getUtilisateurByID(result.user.uid).then((r)=>{
        if(r == undefined){
          this.createUser(result.user.uid,result.user.email,result.user.displayName,result.user.displayName)
        }else{
          this.navCtrl.push(TabsPage);
        }
      });
      
    }).catch((error) => {
      errorCode = error.code;
      errorMessage = error.message;
      this.showError(errorCode,errorMessage)
    });
  }

  createUser(uid,email,nom,prenom){   
    this.db_utilisateur.addDocument2(uid,email,nom,prenom)
    .then(function() {
   })
   this.navCtrl.push(TabsPage);
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
 
  showError(code,text) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: code,
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}