import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

import { credentials } from '../config';
import firebase from 'firebase';

platformBrowserDynamic().bootstrapModule(AppModule);

firebase.initializeApp(credentials.firebase)