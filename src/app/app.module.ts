import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http'
 
import { AboutPage } from '../pages/about/about';
import { CreationTrajetPage } from '../pages/creation-trajet/creation-trajet';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ShowTrajetPage } from '../pages/show-trajet/show-trajet';
import { MesTrajetsPage } from '../pages/mes-trajets/mes-trajets';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalisationProvider } from '../providers/localisation/localisation';
import { TrajetProvider } from '../providers/trajet/trajet';
import { UtilisateurProvider } from '../providers/utilisateur/utilisateur';
import { VilleProvider } from '../providers/ville/ville';
import {Geolocation} from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    CreationTrajetPage,
    HomePage,
    TabsPage,
    ShowTrajetPage,
    TabsPage,
    MesTrajetsPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,   
    HttpClientModule, 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    CreationTrajetPage,
    HomePage,
    TabsPage,
    ShowTrajetPage,
    TabsPage,
    MesTrajetsPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalisationProvider,
    TrajetProvider,
    UtilisateurProvider,
    VilleProvider,
    Geolocation,
    NativeGeocoder
  ]
})
export class AppModule {
}
